package anicks.nl.anwbtest

import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
interface ServiceProvider {
    val githubService: GitHubService

    companion object : ServiceProvider {
        override val githubService: GitHubService by lazy { GitHubService.createInstance() }
    }
}