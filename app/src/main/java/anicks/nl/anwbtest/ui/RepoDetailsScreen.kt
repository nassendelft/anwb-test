package anicks.nl.anwbtest.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.preferredHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.viewModel
import androidx.navigation.NavController
import anicks.nl.anwbtest.Repo
import anicks.nl.anwbtest.ViewModelsFactory
import anicks.nl.anwbtest.ui.theme.ANWBTestTheme
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun RepoDetailsScreen(
    navController: NavController,
    repoId: Long,
    viewModel: RepoViewModel = viewModel("repoDetails", factory = ViewModelsFactory())
) {
    val repos by viewModel.repos.observeAsState(emptyList())
    val repo = repos.find { it.id == repoId }

    if (repo != null) {
        RepoDetails(repo)
    }
}

@Composable
private fun RepoDetails(repo: Repo) {
    val format = SimpleDateFormat("yyyyMMdd", Locale.US)
    val date = try {
        val parsed = format.parse(repo.createdAt)!!
        SimpleDateFormat.getDateInstance().format(parsed)
    } catch (e: ParseException) {
        ""
    }

    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(16.dp)
    ) {
        Text(
            text = repo.name,
            style = MaterialTheme.typography.h3
        )
        Spacer(modifier = Modifier.preferredHeight(8.dp))
        Text(
            text = repo.url,
            style = MaterialTheme.typography.subtitle1
        )
        Spacer(modifier = Modifier.preferredHeight(24.dp))
        Text(
            text = "Default branch: ${repo.defaultBranch}",
            style = MaterialTheme.typography.body1
        )
        Text(
            text = "created at: $date",
            style = MaterialTheme.typography.body1
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    ANWBTestTheme {
        RepoDetails(
            Repo(
                id = 1,
                nodeID = "",
                createdAt = "20210130",
                defaultBranch = "develop",
                name = "test2",
                url = "https://some-url/"
            )
        )
    }
}