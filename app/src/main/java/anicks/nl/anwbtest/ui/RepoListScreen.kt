package anicks.nl.anwbtest.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.savedinstancestate.savedInstanceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import anicks.nl.anwbtest.Repo
import anicks.nl.anwbtest.ViewModelsFactory
import anicks.nl.anwbtest.ui.theme.ANWBTestTheme
import kotlinx.coroutines.launch

@Composable
fun RepoListScreen(
    navController: NavController,
    viewModel: RepoViewModel = viewModel("repoList", factory = ViewModelsFactory()),
) {
    val coroutineScope = rememberCoroutineScope()
    val repos by viewModel.repos.observeAsState(emptyList())

    var loading by remember { mutableStateOf(false) }
    var error: Exception? by remember { mutableStateOf(null) }

    val onSearch: (String) -> Unit = { query ->
        loading = true
        error = null
        coroutineScope.launch {
            try {
                viewModel.fetchRepos(query.filter { !it.isWhitespace() })
            } catch (exception: Exception) {
                error = exception
            }

            loading = false
        }
    }

    Column {
        SearchBar(enabled = !loading, onSearch = onSearch)

        when {
            loading -> {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    CircularProgressIndicator()
                }
            }
            error != null -> {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    Text("Error")
                }
            }
            else -> RepoList(repos) { repo ->
                navController.navigate("repo-details/${repo.id}")
            }
        }
    }
}

@Composable
private fun SearchBar(
    enabled: Boolean = true,
    onSearch: (String) -> Unit
) {
    var searchText by savedInstanceState { "" }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(8.dp)
    ) {
        TextField(
            value = searchText,
            onValueChange = { searchText = it },
            modifier = Modifier.weight(1f),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Go),
            onImeActionPerformed = { action, _ ->
                if (action == ImeAction.Go && enabled) onSearch(searchText)
            }
        )
        Spacer(modifier = Modifier.preferredWidth(8.dp))
        Button(
            enabled = enabled,
            onClick = { onSearch(searchText) }
        ) {
            Text(text = "Go")
        }
    }
}

@Composable
private fun RepoList(
    repos: List<Repo> = emptyList(),
    onItemClick: (Repo) -> Unit = {}
) {
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
    ) {
        items(repos.size) { repo ->
            Row(
                modifier = Modifier
                    .clickable { onItemClick(repos[repo]) }
                    .padding(8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(1f)
                ) {
                    Text(
                        text = repos[repo].name,
                        fontWeight = FontWeight.Bold
                    )
                    Spacer(modifier = Modifier.preferredHeight(8.dp))
                    Text(text = repos[repo].url)
                }
                Spacer(modifier = Modifier.preferredWidth(8.dp))
                Icon(Icons.Filled.ArrowForward, null)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    ANWBTestTheme {
        RepoList(
            listOf(
                Repo(
                    id = 0,
                    nodeID = "",
                    createdAt = "20210130",
                    defaultBranch = "develop",
                    name = "test1",
                    url = "https://some-url/"
                ),
                Repo(
                    id = 1,
                    nodeID = "",
                    createdAt = "20210130",
                    defaultBranch = "develop",
                    name = "test2",
                    url = "https://some-url/"
                )
            )
        )
    }
}