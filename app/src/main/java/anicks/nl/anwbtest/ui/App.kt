package anicks.nl.anwbtest.ui

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import anicks.nl.anwbtest.ViewModelsFactory
import anicks.nl.anwbtest.ui.theme.ANWBTestTheme

@Composable
fun App() {
    ANWBTestTheme {
        Surface(
            color = MaterialTheme.colors.background,
        ) {
            val navController = rememberNavController()

            val vm: RepoViewModel = viewModel("repoDetails", factory = ViewModelsFactory())

            NavHost(navController, startDestination = "repo-list") {
                composable("repo-list") {
                    RepoListScreen(navController, vm)
                }
                composable(
                    "repo-details/{repoId}",
                    arguments = listOf(navArgument("repoId") { type = NavType.LongType })
                ) { backStackEntry ->
                    val repoId = backStackEntry.arguments?.getLong("repoId")!!
                    RepoDetailsScreen(navController, repoId, vm)
                }
            }
        }
    }
}
