package anicks.nl.anwbtest.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import anicks.nl.anwbtest.GitHubService
import anicks.nl.anwbtest.Repo

class RepoViewModel(private val service: GitHubService) : ViewModel() {

    private val _repos = MutableLiveData(emptyList<Repo>())
    val repos: LiveData<List<Repo>> = _repos

    suspend fun fetchRepos(user: String) {
        _repos.value = service.listRepos(user)
    }
}
