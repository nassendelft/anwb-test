@file:Suppress("EXPERIMENTAL_API_USAGE")

package anicks.nl.anwbtest

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import anicks.nl.anwbtest.ui.RepoViewModel

class ViewModelsFactory(private val provider: ServiceProvider) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when(modelClass) {
            RepoViewModel::class.java -> RepoViewModel(provider.githubService)
            else -> error("Could not create ViewModel '${modelClass.name}'")
        } as T
    }

    companion object {
        private var instance: ViewModelsFactory? = null
            get() {
            field = field ?: ViewModelsFactory(ServiceProvider)
            return field
        }

        operator fun invoke() = instance!!
    }
}